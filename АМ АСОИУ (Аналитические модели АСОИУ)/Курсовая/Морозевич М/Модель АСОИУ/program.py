from functools import partial
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter,
                               AutoMinorLocator)
from io import BytesIO
import base64
import numpy as np
import matplotlib.pyplot as plt
import math
import PySimpleGUI as sg

# font-styles
FONT_STYLE = 'Courier'
MEDIUM_FONT = (FONT_STYLE, 15)
SMALL_FONT = ('Helvetica',10)
sg.theme('TealMono')

text_left_alignment = partial(sg.Text, font=MEDIUM_FONT, justification='left')
combo_elem = partial(sg.Combo, font=MEDIUM_FONT, auto_size_text=True)
button = partial(sg.Button, focus=True, visible=True, enable_events=True)
inputtext = partial(sg.InputText, size=(8, 4), justification='right')

fig = plt.figure(figsize=(10, 5))
ax = fig.add_subplot(1, 1, 1)        

b = BytesIO()
plt.savefig(b, format='PNG', dpi=50)
b.seek(0)
b64string = base64.b64encode(b.read())

def initApp():
 
    col_tbl1 = [
        [sg.Text('Время наработки на отказ, ч', size=(30, 1), font=SMALL_FONT)],
        [sg.Text('Время ремонта, ч', size=(30, 1), font=SMALL_FONT)],
        [sg.Text('Количество компьютеров', size=(30, 1), font=SMALL_FONT)],
        [sg.Text('Количество специалистов, вар1', size=(30, 1), font=SMALL_FONT)],
        [sg.Text('Количество специалистов, вар2', size=(30, 1), font=SMALL_FONT)],
        [sg.Text('Количество специалистов, вар3', size=(30, 1), font=SMALL_FONT)],
        [sg.Text('Зарплата специалиста, руб/час', size=(30, 1), font=SMALL_FONT)],
        [sg.Text('Финансовые потери, руб/час', size=(30, 1), font=SMALL_FONT)],
        [sg.Text('Точность', size=(30, 1))],
        ]  
    col_tbl2 = [
        [inputtext(default_text='800', key='ttf_tbl', do_not_clear=True, size=(6, 1))],
        [inputtext(default_text='8', key='t0_tbl', do_not_clear=True, size=(6, 1))],
        [inputtext(default_text='100', key='N_tbl', do_not_clear=True, size=(6, 1))],
        [inputtext(default_text='1', key='c1_tbl', do_not_clear=True, size=(6, 1))],
        [inputtext(default_text='2', key='c2_tbl', do_not_clear=True, size=(6, 1))],
        [inputtext(default_text='3', key='c3_tbl', do_not_clear=True, size=(6, 1))],
        [inputtext(default_text='200', key='S1_tbl', do_not_clear=True, size=(6, 1))],
        [inputtext(default_text='350', key='S_tbl', do_not_clear=True, size=(6, 1))],
        [inputtext(default_text='3', key='r_tbl', do_not_clear=True, size=(6, 1))]
        ]
    col_tbl3 = [
        [sg.Text('c-Количество ремонтников', size=(40, 1), font=SMALL_FONT)],
        [sg.Text('P0-Вероятность, что все компьютеры исправны', size=(40, 1), font=SMALL_FONT)],
        [sg.Text("""Q-Среднее количество компьютеров, находящихся в очереди на ремонт""", size=(40, 2), font=SMALL_FONT)],
        [sg.Text("""L-Среднее количество компьютеров, находящихся в неисправном состоянии""", size=(40, 2), font=SMALL_FONT)],
        [sg.Text("""U-Среднее количество компьютеров, которые непосредственно ремонтируются""", size=(40, 2), font=SMALL_FONT)],
        [sg.Text("""ρ0-Коэффициент загрузки одного специалиста, занятого ремонтом""", size=(40, 2), font=SMALL_FONT)],
        [sg.Text("""n-Среднее количество исправных компьютеров""", size=(40, 1), font=SMALL_FONT)],
        [sg.Text("""ρe-Коэффициент загрузки компьютера""", size=(40, 1), font=SMALL_FONT)],
        [sg.Text("""W-Среднее время нахождения компьютера в очереди на ремонт""", size=(40, 2), font=SMALL_FONT)],
        [sg.Text("""Tp-Среднее время пребывания компьютера в неисправном состоянии""", size=(40, 2), font=SMALL_FONT)],
        [sg.Text("""Tc-Среднее время цикла компьютера""", size=(40, 1), font=SMALL_FONT)],
        [sg.Text("""'ρe/ρ0'-Коэффициент режима работы службы ремонта и обслуживания""", size=(40, 2), font=SMALL_FONT)],
        [sg.Text("""Y-Убытки организации работы службы ремонта компьютеров""", size=(40, 2))],
        ]
    col_tbl4 = [
        [sg.Text('Pрс – Загрузка рабочей станции', size=(40, 1), font=SMALL_FONT)],
        [sg.Text('Pпол – Загрузка пользователя рабочей станции', size=(40, 1), font=SMALL_FONT)],
        [sg.Text("""Kpc – Среднее количество работающих рабочих станций""", size=(40, 1), font=SMALL_FONT)],
        [sg.Text("""Pк – Загрузка канала""", size=(40, 1), font=SMALL_FONT)],
        [sg.Text("""Pпр – Загрузка каждого процессора сервера""", size=(40, 1), font=SMALL_FONT)],
        [sg.Text("""Pд – Загрузка каждого диска сервера""", size=(40, 1), font=SMALL_FONT)],
        [sg.Text("""Tцикла – Среднее время цикла""", size=(40, 1), font=SMALL_FONT)],
        [sg.Text("""Треакц – Среднее время реакции системы""", size=(40, 1), font=SMALL_FONT)],
        [sg.Text("""Lf1 – Начальная интенсивность фонового потока""", size=(40, 1), font=SMALL_FONT)],
        [sg.Text("""LF – Конечная интенсивность фонового потока""", size=(40, 1), font=SMALL_FONT)],
        [sg.Text("""n – Количество итераций""", size=(40, 1), font=SMALL_FONT)],
        ]  
    col_prt4_1 = [
        [sg.Text('Количество рабочих станций ',size=(48, 1))],
        [sg.Text('Среднее время доробатки запроса на PC, ч', size=(48, 1))],
        [sg.Text('Среднее время формирования запроса на PC, ч', size=(48, 1))],
        [sg.Text('Среднее время передачи через канал в прямом направлении', size=(48, 1))],
        [sg.Text('Среднее время передачи через канал в обратном направлении', size=(48, 1))],
        [sg.Text('Количество процессоров', size=(48, 1))],
        [sg.Text('Среднее время обработки запроса на процессоре', size=(48, 1))],
        [sg.Text('Количество дисков', size=(48, 1))],
        [sg.Text('Среднее время обработки запроса на диске', size=(48, 1))],
        [sg.Text('Вероятность обращения запроса к ЦП', size=(48, 1))],
        [sg.Text('K1 = ', size=(8, 1))],
        [sg.Text('K2 = ', size=(8, 1))],
        [sg.Text('Исходная погрешность = ', size=(8, 1))],
        [sg.Text('Точность =', size=(8, 1))],
    ]
    col_prt4_2 = [
        [inputtext(default_text='100', key='N', do_not_clear=True, size=(6, 1))],
        [inputtext(default_text='0', key='T0', do_not_clear=True, size=(6, 1))],
        [inputtext(default_text='1000', key='Tp', do_not_clear=True, size=(6, 1))],
        [inputtext(default_text='5', key='tk1', do_not_clear=True, size=(6, 1))],
        [inputtext(default_text='5', key='tk2', do_not_clear=True, size=(6, 1))],
        [inputtext(default_text='1', key='C', do_not_clear=True, size=(6, 1))],
        [inputtext(default_text='10', key='tnp', do_not_clear=True, size=(6, 1))],
        [inputtext(default_text='2', key='m', do_not_clear=True, size=(6, 1))],
        [inputtext(default_text='20', key='td', do_not_clear=True, size=(6, 1))],
        [inputtext(default_text='0', key='j', do_not_clear=True, size=(6, 1))],
        [inputtext(default_text='0.995', key='K1', do_not_clear=True, size=(6, 1))],
        [inputtext(default_text='100', key='K2', do_not_clear=True, size=(6, 1))],
        [inputtext(default_text='0.05', key='delta', do_not_clear=True, size=(6, 1))],
        [inputtext(default_text='3', key='r', do_not_clear=True, size=(6, 1))]
    ]
    headings = ['Параметры', 'Вариант1', 'Вариант2', 'Вариант3']
    params = ['c', u'P_0', 'Q', 'L', 'U', u'ρ_0', 'n', u'ρ_e', 'W', u'T_p', u'T_ц', u'(ρ_e)/ρ_0', 'Y'] 
    data = [[p] + ['-']*3 for p in params]

    headings_4 = ['Параметры', 'Результат']
    params_4 = ['Pрс =', 
                    'Pпол =', 
                    'Kpc =',
                    'Pк =',
                    'Pпр =', 
                    'Pд =', 
                    'Tцикла =', 
                    'Треакц =', 
                    'Lf1 =', 
                    'LF =', 
                    'n =']
    data_4 = [[p] + ['-'] for p in params_4]
    layout_ttl = [
        [sg.Text("Министерство образования Российской Федерации", font=('Courier', 12), background_color='white')],
        [sg.Text("Московский государственный технический университет им. Н.Э. Баумана", font=('Courier', 12), background_color='white')],
        [sg.Text("", font=('Courier', 6, 'bold'),  background_color='white')],
        [sg.Text("Курсовая работа", font=('Courier', 12, 'bold'),  background_color='white')],
        [sg.Text("""по дисциплине «Аналитические модели автоматизированных систем обработки информации и управления»""", font=('Courier', 12), background_color='white')],
        [sg.Text("", font=('Courier', 6, 'bold'),  background_color='white')],
        [sg.Text("Выполнила:", font=('Courier', 12, 'bold'), background_color='white')],
        [sg.Text("Морозевич М.А.", font=('Courier', 12,), background_color='white')],
        [sg.Text("студентка гр. ИУ5-13М", font=('Courier', 12), background_color='white')],
        [sg.Text("Москва, 2023 г.", font=('Courier', 12), background_color='white')],
        [sg.CButton("Выход")]
    ]
    
    layout_tbl = [
        [
        sg.Column([[text_left_alignment('Исходные данные')],
            [sg.Column(col) for col in [col_tbl1, col_tbl2]]],
             element_justification='left'), 
        sg.Column([[text_left_alignment('Выходные обозначения')],
            [sg.Column(col) for col in [col_tbl3]]],
             element_justification='left'),
        sg.Column([[sg.Table(values=data,
                            headings=headings,
                            select_mode='extended',
                            auto_size_columns=True,
                            justification='right',
                            num_rows=min(len(data), 20),
                            key='table')],
                          ]),
        sg.Column([[sg.Image(data=b64string, key='plot')]]),
        ],
        [sg.RButton('Вычислить', key='calc'),
        sg.RButton('Сохранить', key='save')]]
    layout_tbl_4 =[
        [
        sg.Column([[text_left_alignment('Исходные данные')],
            [sg.Column(col) for col in [col_prt4_1, col_prt4_2]]],
             element_justification='left'), 
        sg.Column([[text_left_alignment('Выходные обозначения')],
            [sg.Column(col) for col in [col_tbl4]]],
             element_justification='left'),
        sg.Column([[sg.Table(values=data_4,
                            headings=headings_4,
                            max_col_width=50,
                            select_mode='extended',
                            auto_size_columns=False,
                            justification='right',
                            num_rows=min(len(data), 20),
                            key='table4')],
                            ]),
        sg.RButton('Вычислить', key='calc4')
    ]]
    layout = [
    [sg.TabGroup([[sg.Tab('Титульный лист', layout_ttl, background_color='White'),
                   sg.Tab('Ремонтник', layout_tbl),
                   sg.Tab('АСОИУ', layout_tbl_4)
                   ]])],
    ]

    window = sg.Window('Модель "Ремонтник"').Layout(layout)
    return window
def HandleEvents(window):
    while True:
        event, values = window.Read()
        if event == 'calc':
            #calc_values(window, values)
            N = int(values['N_tbl'])
            t0 = int(values['t0_tbl'])
            ttf = int(values['ttf_tbl'])
            S1 = int(values['S1_tbl'])
            S = int(values['S_tbl'])
            cs = [int(values[k]) for k in ['c1_tbl', 'c2_tbl', 'c3_tbl']]
            r = int(values['r_tbl'])
            Y = []
            pars = [['c', 'P0', 'Q', 'L', 'U', 'ρ0', 'n', 'ρe', 'W', 'Tp', 'Tc', 'ρe/ρ0', 'Y']]
            for c in cs:
                clc_values = calc_param(ttf, t0, N, S1, S, c, r)
                Y.append(clc_values[-1])
                pars.append(clc_values) 

            plt.cla()
            ax.plot(cs, Y)
            ax.grid(True)
            ax.set_xticks(np.arange(0, max(cs)+1))

            Ydiff = max(Y) - min(Y)
            ax.set_title('График затрат', fontsize=22)
            ax.set_xlabel('Кол-во ремонтников, (человек)', fontsize=18)
            ax.set_ylabel('Затраты, (руб/час)', fontsize=18)
            ax.yaxis.set_major_locator(MultipleLocator(Ydiff // 10 - ((Ydiff // 10) % 5)))
            ax.yaxis.set_minor_locator(MultipleLocator(Ydiff // 20 - ((Ydiff // 20) % 10)))
            ax.set_ylim([min(Y)//10*10-50, max(Y)//10*10+50])
            ax.set_xlim([min(cs)-1, max(cs)+1])
            ax.minorticks_on()
            ax.xaxis.grid(which='major', linestyle='-', linewidth='0.5', color='black', alpha=0.35)
            ax.yaxis.grid(which='major', linestyle='-', linewidth='0.5', color='black', alpha=0.35)
            ax.yaxis.grid(which='minor', linestyle='-', linewidth='0.5', color='black', alpha=0.15)
            b = BytesIO()
            plt.savefig(b, format='PNG', dpi=50)
            b.seek(0)
            b64string = base64.b64encode(b.read())
            window['plot'].update(data=b64string)
            window['table'].update(values=[x for x in zip(*pars)])
        if event == 'save':
            layout = [
                [sg.Text('Введите название файла', size=(10, 1), background_color="white" ), sg.InputText()],
                [sg.Submit(button_text='Готово', size=(15, 1)), sg.Cancel(size=(15, 1))]
            ]
            window = sg.Window('Сохранить', layout, background_color="white")
            saveevent, savevalues = window.Read()
            window.Close()
            if saveevent == 'Готово':
                name_file = savevalues[0]
                if '.' not in name_file[-5:]:
                    plt.savefig(name_file + '.png', format='PNG', dpi=150)
                else:
                    plt.savefig(name_file, dpi=150)
        if event == 'calc4':
            tk1 = float(values['tk1'])
            tk2 = float(values['tk2'])
            C = int(values['C'])
            tnp = float(values['tnp'])
            td = int(values['td'])
            j = float(values['j'])
            N_4 = int(values['N'])
            m = int(values['m'])
            T0 = int(values['T0'])
            Tp = int(values['Tp'])
            delta = float(values['delta'])
            K1 = float(values['K1'])
            K2 = float(values['K2'])
            r_4 = int(values['r'])
            pars_4 = [['Pрс =', 
                     'Pпол =',
                     'Kpc =',
                     'Pк =', 
                     'Pпр =', 
                     'Pд =', 
                     'Tцикла =', 
                     'Треакц =', 
                     'Lf1 =', 
                     'LF =', 
                     'n =']]
            clc_pars4 = calc_param_4(tk1, tk2, C, tnp, td, j, N_4, m, T0, Tp, delta, K1, K2, r_4)
            pars_4.append(clc_pars4)
            window['table4'].update(values=[x for x in zip(*pars_4)]) 
        if event is None or event == 'Exit':
            break

def calc_param(ttf, t0, N, S1, S, c, r):
    #Считаем интенсивности
    mtf = 1/ttf
    m0 = 1/t0
    
    #Считаем коэффициент отказа к восстановлению
    X = mtf/m0
    
    #Посчитаем две половины формулы для базовой вероятности 
    P0_k= []
    P0_summ = 0
    for k in range(0, c+1):
        P0_1 = (math.factorial(N) * (X ** k)) / (math.factorial(k) * math.factorial(N-k))
        P0_summ +=P0_1
        P0_k.append(P0_1)
    for k in range(c+1, N+1):
        P0_2 = (math.factorial(N) * (X ** k)) / ((c**(k - c)) * math.factorial(c) * math.factorial(N - k))
        P0_summ +=P0_2
        P0_k.append(P0_2)
    
    #Посчитаем базовую вероятность
    P0 = (P0_summ)** (-1)
    #print("P0_", c, " = ", P0)

    #Посчитаем вероятности для каждого k
    Pk = []
    for k in range(0, N+1):
        Pk_cur = P0_k[k] * P0
        Pk.append(Pk_cur)
    
    #Рассчитаем среднее количество компьютеров, находящихся в очереди на ремонт
    Q = 0
    for k in range(c+1, N+1):
        Q += (k-c)*Pk[k]
    
    #Рассчитаем среднее количество компьютеров, находящихся в неисправном состоянии
    L = 0
    for k in range(0, N+1):
        L += k * Pk[k]
    
    #Рассчитаем среднее количество компьютеров, которое ремонтируется
    U = L - Q
    
    #Рассчитаем коэффициент загрузки одного специалиста, занятого ремонтом компьютеров
    r0 = U/c
    
    #Рассчитаем среднее время пребывания компьютера в неисправном состоянии
    Tp = (L*ttf)/(N-L)
    
    #Рассчитаем среднее время нахождения компьютера в очереди на ремонт
    W = Tp - t0
    
    #Рассчитаем среднее время цикла для компьютера
    Tc = Tp + ttf
    
    #Рассчитаем коэффициент загрузки компьютера
    re = ttf/Tc
    
    #Рассчитаем среднее количество исправных компьютеров
    n = N - L
    
    #Определяем режим работы службы ремонта и обслуживания компьютеров
    rr = re/P0
    
    #Определим убытки организации в выбранном организации работы службы
    Y = c * S1 + L * S
    return [c, 
            round(P0,r), 
            round(Q,r), 
            round(L,r), 
            round(U,r), 
            round(r0,r), 
            round(n,r), 
            round(re,r), 
            round(W,r), 
            round(Tp,r), 
            round(Tc,r), 
            round(rr,r), 
            round(Y,r)]

def calc_param_4(tk1, tk2, C, tnp, td, j, N_4, m, T0, Tp, delta, K1, K2, r_4):
    b = 1/(1-j)
    Pi = 1/m
    tk = 0.5 * (tk1 + tk2)
    A = 1/(2*tk)
    B = C/(b*tnp)
    H = 1/(b*Pi*td)
    M = min(A, B, H)
    lf1 = K1 * M *((N_4-1) / N_4)
    Tk = (2*tk)/(1 - (2*lf1*tk))
    x = (b * lf1 * tnp) / C
    y = pow(x, C)
    Tnp = (b * tnp) / (1 - y)
    Td = (b*td)/(1 - (b*Pi*lf1*td))
    lf = (N_4-1)/(T0 + Tp + Tk + Tnp + Td)

    lfOld1 = lf1
    n = 1
    while (abs(lfOld1 - lf) / lf) > delta:
        lf_1 = lfOld1 - (lfOld1 - lf) / K2
        Tk = 2 * tk / (1 - 2*lf_1 * tk)
        x = (b * lf_1 * tnp) / C
        y = pow(x, C)
        Tnp = (b * tnp) / (1 - y)
        Td = (b * td) / (1 - (b * Pi * lf_1 * td))
        Tc = T0 + Tp + Tk + Tnp + Td
        lfOld1 = lf_1
        lf = (N_4 - 1) / Tc
        n += 1

    Tc = T0 + Tp + Tk + Tnp + Td
    l = N_4 / Tc
    Ppc = (T0 + Tp)/Tc
    ppol = Tp / Tc
    pk = 2 * l * tk
    pnp = b * l * (tnp/C)
    pd = b * l * Pi * td
    Tr = Tc - Tp
    Kpc=Ppc*N_4
    return [round(Ppc, r_4), 
            round(ppol, r_4), 
            round(Kpc, r_4),
            round(pk, r_4), 
            round(pnp, r_4), 
            round(pd, r_4), 
            round(Tc, r_4), 
            round(Tr, r_4), 
            round(lf1, r_4), 
            round(lf, r_4), 
            round(n, r_4)]
 
if __name__ == '__main__':
    window = initApp()
    HandleEvents(window)