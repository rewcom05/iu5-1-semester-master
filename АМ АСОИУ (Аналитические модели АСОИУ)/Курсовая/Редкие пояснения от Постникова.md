#АМАСОИУ 

Примеры КР > [[АМ АСОИУ (Аналитические модели АСОИУ)/Примеры КР и ДЗ/Скачай меня|Скачай меня]].

- [[Редкие пояснения от Постникова]]
- [[Защита КР и оформление]]
- [[Технические требования к программам + гайды по установке примеров]]

"МАИ" в колонке "Выбор оборудования ЛВС" в варианте - это метод анализа иерархий.

## Приложение 1

Модель роутеров и коммутаторов тоже надо подбирать методами сравнения. Подписать название моделей на схеме.

Укрупненную схему можно построить как в методичке, а можно вторым способом:
![[Укрупненная. Вариант организации сети 2.png]]


## Приложение 2

См. Лекцию ДЗ по Эксплуатации [в архиве](https://gitlab.com/Denactive/iu5-8-semester-bachelor/-/tree/main/Эксплуатация%20%20АСОИУ?ref_type=heads) и на [ОксюморонТВ](https://sites.google.com/site/oksumorontv/эксп-асоииу/). Также можно посмотреть лекции и семинары МППР. В примерах Постникова ([[#Материалы курса]] > от преподавателя) только метод анализа иерархий и взвешенная сумма.

Исходные данные (tно, t0, S1, S) сами придумываем. По варианту только N (кол-во компьютеров) и проверка для c=1/2/3 или 2/3/4.


## КР Часть 3. Ремонтная служба

Часть 3 показывать с компьютера в электронном виде. Сдать в Excel нельзя - не смотрит. 

Печатать эту часть не нужно.

На 1 страницу сделать сравнение ЯП для выполнения моделей хотя бы методом взвешенной суммы.

Базовые требования к программе минимальные - делай, как угодно. Есть [такие требования](https://iu5bmstu.ru/index.php/Постников_В.М.), но я не читаю их актуальными.

Он просит поменять параметры в ваших расчетах - предусмотреть поля ввода.

Требования к программе с моделью ремонтника:  
- вывод графиков:
	- Y - стоимость(затраты) от числа ремонтников C
	- L от числа ремонтников C
	- T_р от числа ремонтников C
- подпись расшифровок обозначений
- поле ввода для числа ремонтников
- поле ввода для числа знаков после запятой в дробных числах

Не уверен, обязательно ли графики нужны в программе. Он просил показать, где выводятся значения L, Т_ц и Y. У меня в табличке выводились. А график у меня только для стоимости был.

**Как принимает**

Вводит значения:
- Тно = 1000
- То=10
- N=100
- при С=1 проверяет, что L=7,5

**Вариант с источником бесперебойного питания**

У кого в КР по амасоиу было необходимо выбрать источник бесперебойного питания? Какие критерии для оценки вы выбрали?

Нужно сравнивать ИБП одного класса, а не разных.

![[Pasted image 20231110133401.png]]


## КР Часть 4. Модель сети + выводы

Нужно показать работающую программу на своём ноутбуке.  
РПЗ лучше тоже напечатать, начиная с аналитического моделирования ЛВС, но распечатку может не посмотреть.. При показе попросит внести входные и выходные данные (которые он задал) в РПЗ.

gpss studio
Путь к GPSS World: C:\Program Files (x86)\Minuteman Software\GPSS World Student Version\GPSS World Student.exe

