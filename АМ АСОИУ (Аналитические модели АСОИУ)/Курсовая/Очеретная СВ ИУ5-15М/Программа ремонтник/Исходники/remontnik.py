import matplotlib.pyplot as plt
from matplotlib.ticker import (MultipleLocator, AutoLocator, AutoMinorLocator)
import base64
from functools import partial
from io import BytesIO
import pyperclip
import PySimpleGUI as sg
from functools import reduce





# //////////////////////////////////////////////////////////////////

FIO = 'Очеретная С.В.'
GROUP = 'ИУ5-15М'
TITULNIK_IMAGE_FILENAME = 'kvT50O41gcM.png'

# //////////////////////////////////////////////////////////////////






def factorial(N):
    if N < 2: return 1
    try:
      return reduce(lambda x, y: x*y, [i for i in range(1, N+1)])
    except Exception as e:
      print('factorial error', e)
      raise e

def get_table_elem(x):
    try:
      if isinstance(x, int):
        return str(x)
      if str(x).isdigit() or '.' in str(x) or '-' in str(x):
        return ('%.4f' % (x)).replace('.', ',')
      return str(x)
    except Exception as e:
      print('get_table_elem error', e)
      raise e

def p0(N, psi, C):
    try:
      pk = [[0] for i in range(N+1)]
      for k in range(C+1):
        pk[k] = factorial(N)*psi**k/(factorial(k)*factorial(N-k))
      for k in range(C+1, N+1):
        pk[k] = factorial(N)*psi**k/(C**(k-C)*factorial(C)*factorial(N-k))
      pk = [elem/sum(pk) for elem in pk]
      return pk
    except Exception as e:
      print('p0 error', e)
      raise e

def q(pk, C, N):
    q = 0
    try:
      for k in range(C+1, N+1):
        q += (k - C)*pk[k]
      return q
    except Exception as e:
      print(pk)
      print(C, N)
      print('q error', e)
      raise e

def l(pk, N):
    l = 0
    try:
      for k in range(1, N+1):
        l += k*pk[k]
      return l
    except Exception as e:
      print('l error', e)
      raise e

def u(l, q):
    return l-q

def rho0(u, C):
    return u/C

def tp(l, N, tno):
    return l*tno/(N-l)

def w(tp, to):
    return tp-to

def tc(tp, tno):
    return tp+tno

def rhoe(Tc, tno):
    return tno/Tc

def n_(N, L):
    return N-L

def rhoe2rho0(rhoe, rho0):
    return rhoe/rho0

def y(L, Sc, S, C):
    return C*Sc+L*S

# font-styles
FONT_STYLE = 'Helvetica'
BIG_FONT = (FONT_STYLE, 18)
MEDIUM_FONT = (FONT_STYLE, 15)
SMALL_FONT = (FONT_STYLE, 12)
TINY_FONT = (FONT_STYLE, 10)

WINDOW_TITLE = 'Автор ' + FIO + ", Группа" + GROUP

sg.theme('LightBlue3')

if __name__ == '__main__':
    # partial functions for some frequent elements to reduce repetitions in arguments
    text_element_left = partial(sg.Text, font=MEDIUM_FONT, justification='left')
    text_element_right = partial(sg.Text, font=MEDIUM_FONT, justification='right')
    button_element = partial(sg.Button, focus=True, visible=True, enable_events=True)
    combo_element = partial(sg.Combo, font=MEDIUM_FONT, auto_size_text=True)



    # //////////////////////////////////////////////////////////////////
    #
    # Страница 1 с титульником
    #
    # //////////////////////////////////////////////////////////////////
    
    def open_title_window():
      layout0 = [
        [
          sg.Column([
            [sg.Image(TITULNIK_IMAGE_FILENAME,
              expand_x=True, expand_y=True,
              key='author_image')],
            ],
            element_justification='center'
          ),
        ]
      ]
      window = sg.Window(WINDOW_TITLE, layout0, modal=True, resizable=True)
      choice = None
      while True:
          event, values = window.read()
          if event == "Exit" or event == sg.WIN_CLOSED:
              break
          
      window.close()







    # //////////////////////////////////////////////////////////////////
    #
    # Страница 2 с графиком
    #
    # //////////////////////////////////////////////////////////////////

    inputtext = partial(sg.InputText, size=(8, 4), justification='right')
    col1 = [[sg.Text('tno - среднее время наработки на отказ одного компьютера')],
            [sg.Text('to - среднее время ремонта одного компьютера')],
            [sg.Text('N - количество компьютеров')],
            [sg.Text('C - количество ремонтников, занятых ремонтом')],
            [sg.Text('С1:')],
            [sg.Text('С2:')],
            [sg.Text('С3:')],
            [sg.Text('Sc - заработная плата специалиста, р/час')],
            [sg.Text('S - финансовые потери организации за неисправный компьютер, р/час')],
            [sg.Text('Точность (кол-во знаков после запятой)')]
            ]

    col2 = [
        [inputtext(default_text='80', key='Tno')],
        [inputtext(default_text='10', key='To')],
        [inputtext(default_text='12', key='N')],
        [inputtext(default_text='2', key='C1')],
        [inputtext(default_text='3', key='C2')],
        [inputtext(default_text='4', key='C3')],
        [inputtext(default_text='300', key='S1')],
        [inputtext(default_text='1000', key='S')],
        [inputtext(default_text='3', key='Acc')],
    ]

    perems = ['C', u'P_0', 'Q', 'L', 'U', u'ρ_0', 'n', u'ρ_e', 'W', u'T_p', u'T_ц', u'(ρ_e)/ρ_0', 'Y']
    descs = [u'количество ремонтников', u'вероятность того, что все компьютеры работают', u'среднее количество компьютеров, находящихся в очереди на ремонт', u'среднее количество компьютеров, находящихся в неисправном состоянии', u'среднее количество ремонтирующихся компьютеров', u'коэффициент загрузки одного специалиста', u'среднее количество исправных компьютеров', u'коэффициент загрузки компьютера', u'среднее время нахождения компьютера в очереди на ремонт', u'среднее время пребывания компьютера в неисправном состоянии', u'среднее время цикла', u'режим работы службы ремонта', u'Убытки организации']
    data = [[p, d, '-', '-', '-'] for p, d in zip(perems, descs)]
    headings = ['Параметры', 'Описание', 'Вар1', 'Вар2', 'Вар3']

    fig1 = plt.figure(figsize=(10, 5))
    ax1 = fig1.add_subplot(1, 1, 1)      

    b = BytesIO()
    plt.savefig(b, format='PNG', dpi=50)
    b.seek(0)
    b64string1 = base64.b64encode(b.read())
    b = BytesIO()
    plt.savefig(b, format='PNG', dpi=50)
    b.seek(0)
    b64string2 = base64.b64encode(b.read())
    b = BytesIO()
    plt.savefig(b, format='PNG', dpi=50)
    b.seek(0)
    b64string3 = base64.b64encode(b.read())




    layout2 = [
              [sg.Column(
                [[sg.Text('Курсовая работа. Очеретная С.В. ИУ5-15М')]],
                element_justification='center'
              )],
              [sg.Column(
                  [[button_element('Показать титульник', key='open_title', size=(20, 3))]]
              )],
              [
                  sg.Column([[sg.Column(col) for col in [col1, col2]],
                          [sg.Table(values=data,
                                        headings=headings,
                                        max_col_width=50,
                                        select_mode='extended',
                                        auto_size_columns=False,
                                        justification='right',
                                        hide_vertical_scroll=True,
                                        vertical_scroll_only=False,
                                        num_rows=min(len(data), 20),
                                        key='table')]
                          ],
                          element_justification='left'), 
                  sg.Column([
                    [sg.Image(data=b64string2, key='plot1')],
                    [sg.Image(data=b64string2, key='plot2')],
                    [sg.Image(data=b64string1, key='plot3')],
                  ])
              ],
              [button_element('Рассчитать', key='calculate', size=(30, 2))]
                # button_element('Сохранить', key='save', size=(40, 3)),
                # button_element('Скопировать', key='copy', size=(40, 3))]]
    ]

    win = sg.Window(WINDOW_TITLE, layout2, default_element_size=(12, 1),
                   resizable=True,finalize=True)
    win.bind('<Configure>',"Event")
    win.Maximize()

    while True:
        events, values = win.read()
        if events is None:  
            break

        if events == 'calculate':
            try:
                N = int(values['N'])
                To = int(values['To'])
                Tno = int(values['Tno'])
                S1 = int(values['S1'])
                S = int(values['S'])
                Cs = [int(values[k]) for k in ['C1', 'C2', 'C3']]
                psi = To/Tno
                acc = int(values['Acc'])
                Y = []
                L_list = []
                Tp_list = []
                perems = [['C', 'p0', 'Q', 'L', 'U', 'ρ0', 'n', 'ρe', 'W', 'Tp', 'Tc', 'ρe/ρ0', 'Y'], descs]
                for C in Cs:
                    pk = p0(N, psi, C)
                    Q = q(pk, C, N)
                    L = l(pk, N)
                    U = u(L, Q)
                    Rho0 = rho0(U, C)
                    Tp = tp(L, N, Tno)
                    W = w(Tp, To)
                    Tc = tc(Tp, Tno)
                    Rhoe = rhoe(Tc, Tno)
                    n = n_(N, L)
                    Re2R0 = rhoe2rho0(Rhoe, Rho0)
                    Y.append(y(L, S1, S, C))
                    L_list.append(L)
                    Tp_list.append(Tp)
                    perems.append(
                        map(lambda x: int(x * pow(10, acc))/pow(10, acc), [C, pk[0], Q, L, U, Rho0, n, Rhoe, W, Tp, Tc, Re2R0, Y[-1]])
                    )

                print(Y)
                print(L_list)
                print(Tp_list)

                plt.cla()
                ax1.grid(True)
                ax1.set_xticks([_ for _ in range(0, max(Cs)+1)])

                ax1.plot(Cs, Y)
                Ydiff = max(Y) - min(Y)
                ax1.set_title('Y - График затрат')
                ax1.set_xlabel('Кол-во ремонтников, человек', fontsize=15)
                ax1.set_ylabel('Затраты, руб/час', fontsize=15)
                ax1.yaxis.set_major_locator(MultipleLocator(Ydiff // 10 - ((Ydiff // 10) % 5)))
                ax1.yaxis.set_minor_locator(MultipleLocator(Ydiff // 20 - ((Ydiff // 20) % 10)))
                ax1.set_ylim([min(Y)//10*10-50, max(Y)//10*10+50])
                ax1.set_xlim([min(Cs)-1, max(Cs)+1])
                ax1.minorticks_on()
                ax1.xaxis.grid(which='major', linestyle='-', linewidth='0.5', color='black', alpha=0.35)
                b = BytesIO()
                plt.savefig(b, format='PNG', dpi=50)
                b.seek(0)
                b64string1 = base64.b64encode(b.read())

                ax1.plot(Cs, L_list)
                Ldiff = max(L_list) - min(L_list)
                ax1.set_title('L - График количества неисправных компьютеров')
                ax1.set_xlabel('Кол-во ремонтников, человек', fontsize=15)
                ax1.set_ylabel('Число неисправных компьютеров', fontsize=15)
                ax1.yaxis.set_major_locator(AutoLocator())
                ax1.yaxis.set_minor_locator(AutoMinorLocator())
                ax1.set_ylim([min(L_list) / 1.1, max(L_list) * 1.1])
                ax1.set_xlim([min(Cs)-1, max(Cs)+1])
                ax1.minorticks_on()
                ax1.xaxis.grid(which='major', linestyle='-', linewidth='0.5', color='black', alpha=0.35)

                b = BytesIO()
                plt.savefig(b, format='PNG', dpi=50)
                b.seek(0)
                b64string2 = base64.b64encode(b.read())

                ax1.plot(Cs, Tp_list)
                Tpdiff = max(Tp_list) - min(Tp_list)
                ax1.set_title('Tp - График времени починки компьютера')
                ax1.set_xlabel('Кол-во ремонтников, человек', fontsize=15)
                ax1.set_ylabel('Затраты, руб/час', fontsize=15)
                ax1.yaxis.set_major_locator(AutoLocator())
                ax1.yaxis.set_minor_locator(AutoMinorLocator())
                ax1.set_ylim([min(Tp_list) / 1.1, max(Tp_list) * 1.1])
                ax1.set_xlim([min(Cs)-1, max(Cs)+1])
                ax1.minorticks_on()
                ax1.xaxis.grid(which='major', linestyle='-', linewidth='0.5', color='black', alpha=0.35)
                b = BytesIO()
                plt.savefig(b, format='PNG', dpi=50)
                b.seek(0)
                b64string3 = base64.b64encode(b.read())
                win['plot1'].update(data=b64string1)
                win['plot2'].update(data=b64string2)
                win['plot3'].update(data=b64string3)

                win['table'].update(values=[x for x in zip(*perems)])
            except Exception as e:
                print('error', e)
        
        if events == 'save':
            layout = [
                [sg.Text('Имя файла', size=(15, 1), background_color="white" ), sg.InputText()],
                [sg.Submit(size=(30, 3)), sg.Cancel(size=(30, 3))]
            ]
            window = sg.Window('Сохранить', layout, background_color="white")
            saveevent, savevalues = window.Read()
            window.Close()

            if saveevent == 'Submit':
                name_file = savevalues[0]
                if '.' not in name_file[-5:]:
                    plt.savefig(name_file + '.png', format='PNG', dpi=150)
                else:
                    plt.savefig(name_file, dpi=150)
        if events == 'copy':
            pyperclip.copy('\n'.join(
                ['\t'.join([get_table_elem(x) for x in row]) 
                for row in win['table'].get()]))
        if events == sg.WIN_CLOSED:
            break
        if events == "open_title":
            open_title_window()

    # need to destroy the window as it's still open
    win.close()
