INITIAL       X$STATION_N,50    ; Кол-во рабочих станций
INITIAL       X$STATION_TD,50.0    ; Среднее время дораб. запр. на ПК
INITIAL       X$STATION_TF,300    ; Средн. время формир. Запр. на ПК
INITIAL       X$CANAL_T,3.0    ; Среднее время перед. через канал
INITIAL       X$SERVER_T,15    ; Средн. вр. обраб. запр. на проц.
INITIAL       X$DISK_N,2    ; Количество дисков
INITIAL       X$DISK_T,30    ; Средн. вр. обраб. запр. на диске
INITIAL       X$PROP,0    ; Вер-ть обращ. запр. к ЦП после д
INITIAL       X$SHAPE,1              ; Форма кривой гамма-распределения
  INITIAL X$LAMBDA,1.013

WORKSTATION_D     STORAGE    50 ; Кол-во РС
WORKSTATION_F     STORAGE    50 ; Также кол-во раб. станций

SERVER  STORAGE      1    ; Кол-во процессоров
; 2 диска
; DISK_N  FUNCTION     RN1,D2 
; .5,1/1,2
; 1 диск                                                                                                                ; Кол-во д.(последн. цифра)
DISK_N  FUNCTION     RN1,D1
1,1

EXPON  FUNCTION     RN1,C23
0,0/.1,.104/.2,.222/.3,.355/.4,.510/.5,.69/.6,.915/.7,1.2/
.75,1.37/.8,1.5/.84,1.83/.88,2.12/.9,2.3/.92,2.52/.94,2.82/
.95,2.98/.96,3.2/.97,3.5/.98,3.9/.995,5.3/.998,6.2/.9995,7/1,8

               GENERATE     ,,,X$STATION_N 
             
WOSF       QUEUE              QSYSTEM
    QUEUE        QFORM
        ENTER            WORKSTATION_F,1
; Выбор распределения
; Экспоненциальное распределение является частным случаем гамма-распределения: Г(1, 1/lambda) = Exp(lambda)
; ADVANCE           Gamma(1, 0, X$STATION_TF,X$SHAPE)
ADVANCE           X$STATION_TF,FN$EXPON
; ADVANCE           (Exponential(1,0,X$STATION_TD))
        LEAVE             WORKSTATION_F,1
    DEPART        QFORM
        ASSIGN              3,SVR

CAN         QUEUE               QCANAL
        SEIZE            CANAL
; Выбор распределения
        ; ADVANCE            (Gamma(1,X$CANAL_T,1,X$SHAPE))
ADVANCE X$CANAL_T,FN$EXPON
; ADVANCE           (Exponential(1,0,X$CANAL_T))
        RELEASE             CANAL
    DEPART           QCANAL
        TRANSFER           ,P3 

SVR         QUEUE        QSERVER
    ENTER         SERVER,1
        ; ADVANCE            (Gamma(1,X$SERVER_T,1,X$SHAPE))
ADVANCE X$SERVER_T,FN$EXPON
; ADVANCE (Exponential(1,0,X$SERVER_T))
        LEAVE              SERVER,1
    DEPART        QSERVER
        ASSIGN       5,FN$DISK_N  
        QUEUE                P5
        SEIZE                P5
        ADVANCE             (Gamma(1,X$DISK_T,1,X$SHAPE)) ; ADVANCE X$DISK_T,FN$EXPON
        RELEASE             P5
        DEPART         P5
        TRANSFER          X$PROP,PER,SVR

PER        ASSIGN              3,WOSD
        TRANSFER          ,CAN 

WOSD       ENTER             WORKSTATION_D,1
        ; ADVANCE            (Gamma(1,X$STATION_TD,1,X$SHAPE))
ADVANCE X$STATION_TD,FN$EXPON
; ADVANCE(Exponential(1,0,X$STATION_TD))
        LEAVE        WORKSTATION_D,1
        DEPART         QSYSTEM
        TRANSFER            ,WOSF  
        GENERATE       5000000
        TERMINATE      1 
        START 1
