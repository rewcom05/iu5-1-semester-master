Подготовка к РК-1. Не пригодилось

0: if / when + present simple >> present simple.
1: if / when + present simple >> will + infinitive
2: _if_ + past simple >> + _would_ + infinitive.
3: _If_ + past perfect >> _would have_ + past participle.
The third conditional is used to imagine a different past. We imagine a change in a past situation and the different result of that change.

> _If I had understood the instructions properly, I would have passed the exam. 
 
> We wouldn't have got lost if my phone hadn't run out of battery.

> _We would have walked to the top of the mountain if the weather hadn't been so bad._


mixed Past perfect/Present:  _If_ + past perfect >> _would_ + infinitive.

Here's a sentence imagining how a change in a past situation would have a result in the present.

> _If I hadn't got the job in Tokyo, I wouldn't be with my current partner._


mixed Present perfect/Past: _If_ + past simple >> _would have_ + past participle.

Here's a sentence imagining how a different situation in the present would mean that the past was different as well.

> _It's really important. If it wasn't, I wouldn't have called you on your holiday._
