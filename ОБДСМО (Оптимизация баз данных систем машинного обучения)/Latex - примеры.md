#ОБДСМО 

Левая группирующая скобка (`begin{cases}`).

$$
\begin{cases}
	    x(n), & \text{for } 0 \leq n \leq 1 \\
	    x(n - 1), & \text{for } 0 \leq n \leq 1 \\
	    x(n - 1), & \text{for } 0 \leq n \leq 1
	\end{cases}
$$

Правая группирующая скобка, центральный отступ

```
\left.
\right\}
```

$$
\begin{equation}
\left.
\begin{array} 2x =X-Y-Z+W , & 2y = -X+Y-Z+W \\ 2z =-X-Y+Z+W , & 2w = X+Y+Z+W \end{array} \right\} \end{equation}
$$

2 группирующие скобки
$$
X(m, n) =
\left.
	\begin{cases}
	    x(n), & \text{for } 0 \leq n \leq 1 \\
	    x(n - 1), & \text{for } 0 \leq n \leq 1 \\
	    x(n - 1), & \text{for } 0 \leq n \leq 1
	\end{cases}
\right\} = xy
$$

Дифференциалы

$$
\begin{equation}
\begin{cases}
&\nabla\times\bar E = -\frac{\partial\bar B}{\partial t}-\bar J_{\text{mi}} \\
&\nabla\times\bar H = \bar J + \bar J_i + \frac{\partial\bar D}{\partial t}\\
&\nabla\cdot\bar J + \nabla\cdot\bar J_i = - \frac{\partial\rho}{\partial t}\\
&\nabla\cdot\bar J_{\text{mi}} = - \frac{\partial\rho_{\text{mi}}}{\partial t}\\
&\nabla\cdot\bar D = \rho\\
&\nabla\cdot\bar B = \rho_m
\end{cases}
\end{equation}
$$

$A \setminus B$ - `\setminus`

$Q = A$, $X \in (\cancel{CE}, \cancel{B}, BCDE)$ - `\cancel{B}`



Распределено по закону

$x  \overset{N}{\sim} N(0,1)$ - `\overset{N}{\sim}`
$x \stackrel{N}{\sim}$  - `\stackrel{N}{\sim}`
или просто
$x \sim$  - `\sim`


Греческий алфавит

| Название буквы | Команды                 | Результат                   |
|----------------|-------------------------|-----------------------------|
| Альфа          | A \alpha                | $A\ \alpha$                 |
| Бета           | B \beta                 | $B\ \beta$                  |
| Гамма          | \Gamma \gamma           | $\Gamma\ \gamma$            |
| Дельта         | \Delta \delta           | $\Delta\ \delta$            |
| Эпсилон        | E \epsilon \varepsilon  | $E\ \epsilon\ \varepsilon$  |
| Дзета          | Z \zeta                 | $Z\ \zeta$                  |
| Эта            | H \eta                  | $H\ \eta$                   |
| Тета           | \Theta \theta \vartheta | $\Theta\ \theta\ \vartheta$ |
| Йота           | I \iota                 | $I\ \iota$                  |
| Каппа          | K \kappa \varkappa*     | $K\ \kappa\ \varkappa$      |
| Лямбда         | \Lambda \lambda         | $\Lambda\ \lambda$          |
| Мю (ми)        | M \mu                   | $M\ \mu$                    |
| Ню (ни)        | N \nu                   | $N\ \nu$                    |
| Кси            | \Xi \xi                 | $\Xi\ \xi$                  |
| Омикрон        | O o                     | $O\ o$                      |
| Пи             | \Pi \pi \varpi          | $\Pi\ \pi\ \varpi$          |
| Ро             | P \rho \varrho          | $P\ \rho\ \varrho$          |
| Сигма          | \Sigma \sigma \varsigma | $\Sigma\ \sigma\ \varsigma$ |
| Тау            | T \tau                  | $T\ \tau$                   |
| Ипсилон        | \Upsilon \upsilon       | $\Upsilon\ \upsilon$        |
| Фи             | \Phi \phi \varphi       | $\Phi\ \phi\ \varphi$       |
| Хи             | X \chi                  | $X\ \chi$                   |
| Пси            | \Psi \psi               | $\Psi\ \psi$                |
| Омега          | \Omega \omega           | $\Omega\ \omega$            |



$\tilde{A}$ - `\tilde{A}`
$\widetilde{x}$ - `\widetilde{x}`
$\bar{ABC}$ - `\bar{ABC}`
$\overline{ABC}$ - `\overline{ABC}`