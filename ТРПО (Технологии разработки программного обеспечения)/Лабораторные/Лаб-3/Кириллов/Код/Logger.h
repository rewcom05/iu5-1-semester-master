/**
    @file Logger.h
    ���� ������������ ���� � �����������
    @author �������� �.�.
    @date 03-10-2023 17:06
    \par ���������� �����:
    - @ref Logger
*/

#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <mutex>

namespace Logging
{
    /// ����� ������� ��� ����������� ����������� � ������������� ������
    /**
    ������ ����� ��� ������ � ����. ����������� ��������� � ���� � �������� RAII. ����������, � �� �������� ������ ��������� �� ���� ����� ��� ����������� ����������
    */
    class MyLogger
    {
    public:
        /*
        explicit MyLogger(std::string fileName);
        ~MyLogger();
        explicit MyLogger(MyLogger&& other);
        explicit MyLogger(const MyLogger&) = delete;
        MyLogger& operator=(MyLogger&& other);
        MyLogger& operator= (const MyLogger&) = delete;
        void WriteLine(std::string content);
        */

 
        /// ����������� ����������� ������
        explicit MyLogger(const MyLogger&) = delete;
        /// �������� ����������� ������
        MyLogger& operator= (const MyLogger&) = delete;
        
        /// ����������� ��������
        MyLogger()
        {
            std::cout << "Logger ������" << std::endl;
        };

        /// move-�����������
        /** ������� ������
        \param other ������� ������ �� ��������� ������
        */
        MyLogger(MyLogger&& other)
        {
            mStream.close();
            mStream = std::move(other.mStream);
        }

        /// move-�������� ����������
        /**
        \param other ������� ������ �� ��������� ������
        */
        MyLogger& operator=(MyLogger&& other)
        {
            mStream.close();
            mStream = std::move(other.mStream);

            return *this;
        }

        ~MyLogger()
        {
            mStream.close();
        }

        /// ������ ������ � ���
        /**
        \param content ������ ������
        \return ������ "ok"
        */
        void WriteLine(std::string content): const char* {
            mStream << content << std::endl;
            return "ok"
        };

    protected:
        std::ofstream mStream;
        std::mutex mMutex;
    };


    /**
    �� ��, ��� MyLogger, �� ����� � ���� � ������ ������, ���� ���� ���������� ��� ������
    */
    class FileLogger: public MyLogger {
        /// ����������� ��������
        /**
        @brief ������� ����-������
        @param fileName ��� ����� � �����
        @throw "�� ������� ������� ���� ��� ����� - �������������" ���� �� ������� ������� ����
        */
    public:
        FileLogger(std::string fileName)
        {
            mStream.open(fileName, std::ios::app);
            if (!mStream.is_open()) {
                throw "�� ������� ������� ���� ��� ����� - �������������";
            }
        };
    };
}