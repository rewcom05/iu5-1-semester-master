const ll2 = new LinkedList();

// Имитация долгих расчетов
const delay = ms => new Promise(res => setTimeout(res, ms));
const loadCpu = (n) => {
  return Array(n).reduce((acc) => {acc += Math.random()}, 0);
}


const output = document.getElementById('ll');



const addValueInput = document.getElementById('add');
const addBtn = document.getElementById('add-btn');

addBtn.addEventListener('click', async () => {
  const value = addValueInput.value;

  if (value !== undefined && value !== null && value !== "") {
    await delay(100);
    ll2.add(value);
    addValueInput.value = "";

    output.innerHTML = ll2.getContent();
  }
});



const insertIndexInput = document.getElementById('insert-index');
const insertValueInput = document.getElementById('insert-element');
const insertBtn = document.getElementById('insert-btn');

insertBtn.addEventListener('click', async () => {
  const index = insertIndexInput.value;
  const value = insertValueInput.value;

  if (value !== undefined && value !== null && value !== "" && index !== "") {
    loadCpu(10000);
    ll2.insertAt(value, index);
    insertIndexInput.value = "";
    insertValueInput.value = "";

    output.innerHTML = ll2.getContent();
  }
});
