// const {LinkedList} = require('./linkedList.js');

const ll = new LinkedList();

ll.add(1);
ll.add(2);
ll.add(4);
ll.insertAt(3, 2);

console.log(ll.toString()); // 1,2,3,4

ll.removeElement(2);
ll.removeElement(4);
ll.removeFrom(1);

console.log(ll.toString()); // 1
