const {LinkedList, Node} = require('../linkedList.js');

describe('LinkedList test suites', () => {
  test('getContent', () => {
    const ll = new LinkedList();
    expect(ll.head).toBe(null);
    expect(ll.size).toBe(0);
    
    const node2 = new Node(2);
    const node1 = new Node(1);
    node1.next = node2;
    
    ll.head = node1;
    ll.size = 2;

    const content = ll.getContent();
    expect(content).toMatchObject([1,2]);
  });

  test('insert / insertAt / remove / removeFrom', () => {
    const ll = new LinkedList();
    ll.add(1);
    ll.add(2);
    ll.add(4);
    ll.insertAt(3, 2);
    
    expect(ll.getContent()).toMatchObject([1,2,3,4]);
    
    ll.removeElement(2);
    ll.removeElement(4);
    ll.removeFrom(1);
    
    expect(ll.getContent()).toMatchObject([1]);
  });
})
