#ТРПО 

( ! ) [[Приемы ООП.pdf]] - книжка для ботания паттернов GOF


Определение узлов и сетевых конфигураций - это диаграмма развертывания
Определение подсистем и их интерфейсов - это диаграмма уровней подсистем. Подсистема на этапе реализации - это проект (с++), пакет (java) (короче приложение)
- Определение подсистем специального прикладного уровня: интерфейс пользователя
- Определение подсистем общего прикладного уровня: апи, логика на сервере
- Определение подсистем среднего уровня: СУБД, фреймворки, nginx/apache
- Определение подсистем уровня системного ПО: протокол TCP

артефакты - файлы на выходе после этапа реализации: исполняемые файлы (.cpp, cs, js), exe-шники, "билды", набор тестов, план тестов, примеры для тестов, отчеты
билд - работающая версия всего проекта (M exe-шников) внутри 1 итерации RUP. 1 итерация -> N билдов).


Структурные шаблоны MVC, PCMEF и PCMEF+ описывают организацию пакетов и их взаимодействие.


Все диаграммы из лекций
Этап анализа.
- Диаграмма взаимодействия
- Диаграммы классов анализа: граничные, управляющие, сущностей
- Диаграмма последовательностей кооперации
- Диаграмма пакетов. Размещение классов анализа: граничные, управляющие, сущностей.
Этап проектирования.
- Диаграмма развертывания. Распределение подсистем по узлам.
- Диаграмма уровней подсистем. Зависимости и интерфейсы подсистем.
- Диаграмма пакетов. Распределение классов по подсистемам.
- Диаграмма последовательности объектов
- Диаграмма классов проектирования
Реализация подсистемы
- Диаграмма последовательностей в терминах компонентов.
- Диаграммы последовательностей в терминах подсистем для коопераций.
- Диаграмма развертывания. Размещение компонентов.
- Диаграмма компонентов. Трассировка подсистем в компоненты.
- Диаграмма классов. Распределение классов по подсистемам

Какие диаграммы рисовать?

Пункт 1. Выделить для ИС/АСУ подсистемы

По идее, диаграмма пакетов.
Пакеты на этапе проектирования трассируются в подсистемы, затем на
этапе реализации трассируются в компоненты.
Диаграмма пакетов напрямую связана с диаграммой компонентов, но на диаграмме компонентов не отмечаются классы, а это требуется в заданиях.


Схема для архитектуры каркасов?
по идее так, что у тебя появляются "пакеты из фреймворка". Для джанги это будет ORM-подсистема, подсистема-шаблонов, подсистема роутинга, подсистема-вьюх  
Типа то, что ты можешь использовать или не использовать по желанию





прецеденты = кооперации



Чем паттерн знакомство отличается от паттерна пакет знакомств? Знакомство между соседними классами, в пакет знакомства переносят не соседние знакомства. Вроде так.



Какой-то паттерн. Вроде "Компонентная архитектура".
Есть набор обязательных интерфейсов для выполнения подключения - это стандартная архитектура связи. Далее компоненты (или подсистемы) предлагают свой набор функций, но через стандартные интерфейсы они, во-первых, регистрируют себя, оповещают о тех операциях, которые они выполняют, и затем промежуточное ПО получает заявку на выполнение интерфейса, находит нужный компонент, и через его стандартный интерфейс передает данные. Компонент кроме реализации стандартного интерфейса может делать все что угодно

datamapper принимает на вход и отдает на выход объекты предметной области. Если работать с коллекциями (массивами) строк БД, то это table gateway.

service-layer
При разбиении на сервисы можно дойти до сценария транзакций. Должна быть прослойка между верхнем уровнем и ... (какими-то сервисными функциями и подсистемами).

При разбиении на сервисы нужно знать ответ на вопрос, а чем ваше разбиение отличается от реализации table module и сценария транзакций. Короче, (мое предположение) 1 сервис должен работать сразу с несколькими таблицами.

В ДЗ-2 должно суммарно получиться порядка 15 классов. Они должны работать с основными классами из диаграммы сущностей. Получится типа по 5 таблиц на каждом уровне.




РК-2

1-й пункт... не записал

2-й пункт. Отрисовать пакеты, указать, в каких пакетах какие есть классы, у классов - какие атрибуты, какие методы, и кто кого вызывает, определить взаимодействие пакетов.
В этом пункте либо паттерн "публикация" (издатель-подписчики), либо делегирование. Либо нарисовать, либо текстом написать: такой-то класс делегирует метод...

3-й пункт - паттерны GOF. будет 2 паттерна GOF. Мы должны нарисовать классы и нарисовать диаграмму классов и диаграмму последовательностей при выполнении одного из главных прецедентов с учетом вашего паттерна. Это все в меру вашей фантазии. Можно делать пояснение текстом кратко.

4-й и 5-й пункты. Виноградова считает их "простенькими". Просто привести примеры функций, написать код или объяснить, что делает эта функция на 2-3 предложения, указать особенности.

Оценка РК-2 - сам РК до 10 баллов + 2 балла за срок + 3 балла доп. 2 недели на проверку (защиту).
Если что-то недорисовано, есть шанс объяснить идею на защите. Тогда будут вопросы по теории. Если чего-то нет совсем, то срок сохраняется, но через 2 недели попадаете на переписывание.






Переписывание
- Пункты 1-2 можно сделать дома и принести готовые: архитектура модели + структурные шаблоны. Показать картинки. Ответить на вопросы по теории.
- Пункт 3. Дается паттерн и 30 мин. Нужно этот паттерн нарисовать.

1 попытка переписывания идет с сохранением срока. Все что дальше пойдет на минимальный балл.


