#ТРПО 

## Материалы

- Table Module (C#) - PATTERNS OF ENTERPRISE ARCHITECTURE https://www.youtube.com/watch?v=rMedC-2HsO0 
- System.Data.SqlClient: Выполнение команд и SqlCommand https://metanit.com/sharp/adonet/2.5.php
- Методы Npgsql, библиотека Dapper, Entity Framework - https://michaelscodingspot.com/postgres-in-csharp/

## Паттерны

## Библиотеки

### System.Data.SqlClient и Npgsql

Для обычных баз (MySQL и т.д.) есть системный пакет System.Data.SqlClient.
Для постгрес есть пакет Npgsql.

Пакеты имеют в зависимостях версию языка .NetCore.


#### connectionString

Выглядят по-разному:

В System.Data.SqlClient.SQLConnection connectionString имеет синтаксис:
```C#
var connectionString = "Server=localhost;UserId=postgres;Password=pass;Database=dbname";
```

В Npgsql:
```C#
var connectionString = "Host=localhost;Username=postgres;Password=pass;Database=dbname";
```


#### Прочитать табличку

System.Data.SqlClient 
```C#
connection.Open();
using var cmd = new NpgsqlCommand();
cmd.CommandText = $"SELECT * FROM { _tableName}";
NpgsqlDataReader reader = await cmd.ExecuteReaderAsync();
// Нужен конструктор для объектов. Нужно итерировать строки и перобразовывать типы.
var result = new List<Teacher>();
    while (await reader.ReadAsync())
    {
        result.Add(new Teacher(
            id: (int)reader["id"],
            first_name: reader[1] as string, // column index can be used
            last_name: reader.GetString(2), // another syntax option
            subject: reader["subject"] as string,
            salary: (int)reader["salary"]));

    }
    return result;
```

System.Data.SqlClient SqlDataAdapter - возвращает коллекцию данных DataTable. SQLAdapter нет в ранних версиях пакета System.Data.SqlClient

```C#
connection.Open();
SqlDataAdapter adapter = new SqlDataAdapter();
DataTable table = new DataTable();
adapter.Fill(table);
return table;
```

Npgsql NpgsqlDataAdapter
```C#
connection.Open();
NpgsqlDataAdapter da = new NpgsqlDataAdapter($"SELECT * FROM { _tableName}", _connectionString);
DataTable table = new DataTable();
da.Fill(table);
postgresql_dataGrid.DataSource = myTable.DefaultView;
return table;
```


### Entity Framework

См. матегриалы

### Dapper

Для C#.
Упрощает реализауию паттерна Domain Model.


## SQL

Просмотр таблиц в базе данных
```SQL
SELECT TABLE_NAME
FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_TYPE = 'BASE TABLE' AND TABLE_CATALOG = 'dbName';
```

## Ошибки

### Отношение не определено

Хотя таблица точно существует: в pgAdmin таблица присутствует
Причина: нет прав на public доступ.
```SQL
GRANT ALL PRIVILEGES ON DATABASE dbname TO username;
```


### 42p01 отношение не существует

Причина: не нравится регистр SQL-запроса.

Создал таблицу с названием в кавычках:
```SQL
CREATE TABLE "USERS"
(
	"ID" serial PRIMARY KEY NOT NULL,
	"LOGIN" varchar(255) NOT NULL,
	"PASSWORD" varchar(255) NOT NULL,
	"FIO" varchar(255) NOT NULL
)
;
```

Попытался обратиться в коде:
```C#
var _tableName = "USERS";
NpgsqlDataAdapter da = new NpgsqlDataAdapter($"SELECT * FROM { _tableName}", _connectionString);
```

Постгрес перевел в lowercase - таблицы с таким именем не существует. Нужно сохранить регистр вот так:
```C#
var _tableName = "\"USERS\""
```
Еще как-то можно с помощью знака апострофа ( \` ).
