﻿using System;
using Npgsql;
using TableModule.Entities;
using TableModule.Modules;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var connectionString = "Host=localhost;Username=postgres;Password=1407;Database=trpo";

                using var connection = new NpgsqlConnection(connectionString);
                connection.Open();
                var cmd = new NpgsqlCommand("SELECT version()", connection);
                var version = cmd.ExecuteScalar().ToString();
                Console.WriteLine($"PostgreSQL version: {version}");


                UserModule userModule = new UserModule(connectionString);
                RoleModule roleModule = new RoleModule(connectionString);
                ContributorModule contributorModule = new ContributorModule(connectionString);
                JointRepositoriesContributorsModule jrcModule = new JointRepositoriesContributorsModule(connectionString, contributorModule);
                RepositoryModule repositoryModule = new RepositoryModule(connectionString, contributorModule, roleModule, jrcModule);

                Console.WriteLine("Таблица USERS до выполнения скрипта");
                Console.WriteLine(userModule.ToString());
                Console.WriteLine();

                Console.WriteLine("Таблица ROLES до выполнения скрипта");
                Console.WriteLine(roleModule.ToString());
                Console.WriteLine();

                Console.WriteLine("Таблица CONTRIBUTORS до выполнения скрипта");
                Console.WriteLine(contributorModule.ToString());
                Console.WriteLine();

                Console.WriteLine("Таблица REPOSITORIES до выполнения скрипта");
                Console.WriteLine(repositoryModule.ToString());
                Console.WriteLine();

                Console.WriteLine("Таблица REPOSITORIES_CONTRIBUTORS до выполнения скрипта");
                Console.WriteLine(jrcModule.ToString());
                Console.WriteLine();


                Console.WriteLine("Пример 1. Поиск записи о пользователе");
                User dskirillov = userModule.FindByLogin("dskirillov");
                Console.WriteLine("Информация о Кириллове: " + dskirillov.ToString());
                Console.WriteLine();

                Console.WriteLine("Пример 2. Поиск роли developer по названию");
                Role developerRole = roleModule.FindByName("developer");
                Console.WriteLine("Поиск новой записи " + developerRole.ToString());
                Console.WriteLine();

                Console.WriteLine("Пример 3. Создание репозитория");
                repositoryModule.Create(new Repository() { name = "Репозиторий №2" });
                Console.WriteLine("Таблица REPOSITORIES после создание репозитория №2");
                Console.WriteLine(repositoryModule);
                Console.WriteLine();


                Console.WriteLine("Пример 4. Добавление contributor'a в репозиторий #2");
                Repository repository = repositoryModule.FindByName("Репозиторий №2");
                Console.WriteLine("Информация о репозитории: " + repository);
                repositoryModule.AddContributor(repository, dskirillov, developerRole);
                Console.WriteLine("Таблица CONTRIBUTORS после добавления developer'а");
                Console.WriteLine(contributorModule);
                Console.WriteLine("Таблица REPOSITORIES_CONTRIBUTORS после добавления contributor'а");
                Console.WriteLine(jrcModule);
                var repoContributors = repositoryModule.GetContributors(repository.id);
                Console.WriteLine("Получение списка contributor'ов repository");
                for (int i = 0; i < repoContributors.Length; i++)
                {
                    Console.WriteLine($"{i}: {repoContributors[i]}");
                }
                Console.WriteLine();

                Console.WriteLine("Пример 5. Удаление developer'а");
                repositoryModule.RemoveContributor(repository, dskirillov);
                Console.WriteLine("Таблица CONTRIBUTORS после удаления developer'а");
                Console.WriteLine(contributorModule);
                Console.WriteLine();

                Console.WriteLine("Пример 6. Удаление репозитория №2");
                repositoryModule.Delete(repository);
                Console.WriteLine("Таблица REPOSITORIES после удаления репозитория №2");
                Console.WriteLine(repositoryModule);
                Console.WriteLine();
            }
            catch (Exception e)
            {
                Console.WriteLine($"Processing failed: {e.Message}");
                throw;
            }
        }
    }
}
