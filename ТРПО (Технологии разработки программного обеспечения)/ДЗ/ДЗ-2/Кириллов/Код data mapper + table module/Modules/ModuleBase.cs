﻿using TableModule.Entities;

namespace TableModule.Modules
{
    public abstract class ModuleBase<Mapper, Entity> where Mapper : Mappers.MapperBase<Entity> where Entity : EntityBase
    {
        protected Mapper _mapper;

        protected ModuleBase(Mapper mapper)
        {
            _mapper = mapper;
        }

        protected Entity GetRow(int id)
        {
            return _mapper.Read(id);
        }
        
        protected Entity[] GetTable()
        {
            return _mapper.ReadAll();
        }

        public override string ToString()
        {
            return _mapper.ToString();
        }
    }
}