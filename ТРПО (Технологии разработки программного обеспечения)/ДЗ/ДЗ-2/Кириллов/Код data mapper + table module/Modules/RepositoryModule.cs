﻿using System;
using TableModule.Entities;

namespace TableModule.Modules
{
    public class RepositoryModule : ModuleBase<Mappers.RepositoryMapper, Repository>
    {
        JointRepositoriesContributorsModule _jrcModule;
        ContributorModule _contributorModule;
        RoleModule _roleModule;

        public RepositoryModule(string connectionString, ContributorModule contributorModule, RoleModule roleModule, JointRepositoriesContributorsModule jrcModule) : base(new Mappers.RepositoryMapper(connectionString)) {
            _jrcModule = jrcModule;
            _contributorModule = contributorModule;
            _roleModule = roleModule;
        }
        
        public int Create(Repository repository)
        {
            return _mapper.Create(repository);
        }

        public int Delete(Repository repository)
        {
            return _mapper.Delete(repository);
        }

        public Repository FindByName(string name)
        {
            return _mapper.FindByName(name);
        }

        public int AddContributor(Repository repository, User user, Role role)
        {
            // Проверка, если уже есть такой пользователь
            var contributor = FindContributorByUserId(repository.id, user.id);
            if (contributor != null)
            {
                Console.WriteLine($"Contributor {user.id} уже есть");
                return 0;
            }

            contributor = new Contributor() { userId = user.id, roleId = role.id };
            _contributorModule.Create(contributor);
            var jrc = new JointRepositoriesContributors()
            {
                repositoryId = repository.id,
                contributorId = contributor.id,
            };
            return _jrcModule.Create(jrc);
        }

        public int RemoveContributor(Repository repository, User user)
        {
            var contributor = FindContributorByUserId(repository.id, user.id);
            if (contributor != null)
            {
                var jrc = _jrcModule.Find(repository.id, contributor.id);
                _jrcModule.Delete(jrc);
                return _contributorModule.Delete(contributor);
            }
            return 0;
        }

        public Contributor FindContributorByUserId(int repoId, int userId)
        {
            var contributors = GetContributors(repoId);
            for (int i = 0; i < contributors.Length; i++)
            {
                var contributor = contributors[i];
                if (contributor.userId == userId)
                {
                    return contributor;
                }
            }
            return null;
        }

        public Contributor[] GetContributors(int repoId)
        {
            return _jrcModule.GetRepositoryContributors(repoId);
        }

        public Contributor GetManager(int repoId)
        {
            var contributors = GetContributors(repoId);
            var managerRoleId = _roleModule.FindByName("managerRole").id;
            for (int i = 0; i < contributors.Length; i++)
            {
                if (contributors[i].roleId == managerRoleId)
                {
                    return contributors[i];
                }
            }
            return null;
        }
    }
}
