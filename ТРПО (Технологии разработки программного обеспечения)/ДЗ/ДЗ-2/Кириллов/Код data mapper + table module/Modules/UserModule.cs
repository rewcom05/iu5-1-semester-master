﻿using TableModule.Entities;

namespace TableModule.Modules
{
    public class UserModule : ModuleBase<Mappers.UserMapper, User>
    {
        public UserModule(string connectionString) : base(new Mappers.UserMapper(connectionString)) {}
        
        public User FindByLogin(string login)
        {
            return _mapper.FindByLogin(login);
        }
    }
}
