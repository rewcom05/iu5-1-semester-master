﻿using TableModule.Entities;

namespace TableModule.Modules
{
    public class RoleModule : ModuleBase<Mappers.RoleMapper, Role>
    {
        public RoleModule(string connectionString) : base(new Mappers.RoleMapper(connectionString)) { }
        
        public Role FindByName(string name)
        {
            return _mapper.FindByName(name);
        }

        public string RoleToString(int roleId)
        {
            return GetRow(roleId).name;
        }
    }
}
