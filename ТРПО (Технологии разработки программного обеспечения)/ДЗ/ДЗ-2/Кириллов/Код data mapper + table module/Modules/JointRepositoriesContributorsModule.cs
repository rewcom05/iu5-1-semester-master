﻿using TableModule.Entities;
using System;

namespace TableModule.Modules
{
    public class JointRepositoriesContributorsModule : ModuleBase<Mappers.JointRepositoriesContributorsMapper, JointRepositoriesContributors>
    {
        private ContributorModule _contributorModule;
        public JointRepositoriesContributorsModule(string connectionString, ContributorModule contributorModule) : base(new Mappers.JointRepositoriesContributorsMapper(connectionString))
        {
            _contributorModule = contributorModule;
        }
        
        public int Create(JointRepositoriesContributors jrc)
        {
            return _mapper.Create(jrc);
        }

        public int Delete(JointRepositoriesContributors jrc)
        {
            return _mapper.Delete(jrc);
        }

        public JointRepositoriesContributors Find(int repoId, int contributorId) {
            return _mapper.Read(repoId, contributorId);
        }

        public Contributor[] GetRepositoryContributors(int repoId)
        {
            var contributorIds = _mapper.GetRepositoryContributors(repoId);
            var contributors = new Contributor[contributorIds.Length];
            for (int i = 0; i < contributors.Length; i++)
            {
                try
                {
                    contributors[i] = _contributorModule.Find(contributorIds[i]);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error while getting repository contributors", e);
                    contributors[i] = null;
                }
            }
            return contributors;
        }
    }
}
