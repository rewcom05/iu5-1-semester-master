﻿using TableModule.Entities;

namespace TableModule.Modules
{
    public class ContributorModule : ModuleBase<Mappers.ContributorMapper, Contributor>
    {
        public ContributorModule(string connectionString) : base(new Mappers.ContributorMapper(connectionString)) { }
        
        public int Create(Contributor contributor)
        {
            return _mapper.Create(contributor);
        }

        public int Delete(Contributor contributor)
        {
            return _mapper.Delete(contributor);
        }

        public Contributor Find(int contributorId)
        {
            return _mapper.Read(contributorId);
        }

        public Contributor FindByUserIdAndRole(int roleId, int userId)
        {
            return _mapper.FindByUserIdAndRole(roleId, userId);
        }
    }
}
