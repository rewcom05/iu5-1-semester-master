﻿using System.Text;
using System.Data;

namespace TableModule
{
    public static class Utils
    {
        public static string DataTableToString(DataTable table)
        {
            string data = string.Empty;
            StringBuilder sb = new StringBuilder();

            if (null != table && null != table.Rows)
            {
                foreach (DataRow dataRow in table.Rows)
                {
                    sb.Append(DataRowToString(dataRow));
                    sb.AppendLine();
                }
                data = sb.ToString();
            }
            return data;
        }

        public static string DataRowToString(DataRow dataRow)
        {
            string data = string.Empty;
            StringBuilder sb = new StringBuilder("|");

            if (null != dataRow)
            {
                foreach (var item in dataRow.ItemArray)
                {
                    sb.Append(" ");
                    sb.Append(item);
                    sb.Append(" |");
                }
            }
            data = sb.ToString();
            return data;

        }
        public static string ColumnNamesToString(DataTable table)
        {
            string data = string.Empty;
            StringBuilder sb = new StringBuilder();

            foreach (DataColumn column in table.Columns)
            {
                sb.Append(column.ColumnName);
                sb.Append(',');
            }
            data = sb.ToString();
            return data;
        }
    }
}
