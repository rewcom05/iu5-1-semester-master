﻿using System;
using System.Data;

namespace TableModule.Entities
{
    public class Contributor: EntityBase
    {
        public int id = -1;
        public DateTime created = DateTime.MinValue;
        public int roleId = -1;
        public int userId = -1;

        public Contributor() : base() { }

        public Contributor(DataRow dr): base()
        {
            id = dr.Field<int>("ID");
            created =dr.Field<DateTime>("CREATED");
            roleId = dr.Field<int>("ROLE_ID");
            userId = dr.Field<int>("USER_ID");
            Init();
        }

        public override string ToString()
        {
            return $"[Contributor] {id} {created} {roleId} {userId}";
        }
    }
}
