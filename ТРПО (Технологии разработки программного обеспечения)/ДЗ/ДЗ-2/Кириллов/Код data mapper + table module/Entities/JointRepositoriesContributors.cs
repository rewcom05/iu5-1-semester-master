﻿using System.Data;

namespace TableModule.Entities
{
    public class JointRepositoriesContributors: EntityBase
    {
        public int contributorId = -1;
        public int repositoryId = -1;

        public JointRepositoriesContributors() : base() { }

        public JointRepositoriesContributors(DataRow dr) : base()
        {
            contributorId = dr.Field<int>("CONTRIBUTORS_ID");
            repositoryId = dr.Field<int>("REPOSITORIES_ID");
            Init();
        }

        public override string ToString()
        {
            return $"[JointRepositoriesContributors] {contributorId} {repositoryId}";
        }
    }
}
