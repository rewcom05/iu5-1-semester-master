﻿using System.Data;

namespace TableModule.Entities
{
    public class Repository: EntityBase
    {
        public int id = -1;
        public string name = "";

        public Repository() { }

        public Repository(DataRow dr)
        {
            id = dr.Field<int>("ID");
            name = dr.Field<string>("NAME");
            Init();
        }

        public override string ToString()
        {
            return $"[Repository] {id} {name}";
        }
    }
}
