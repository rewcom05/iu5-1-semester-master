﻿using System.Data;

namespace TableModule.Entities
{
    public class Role: EntityBase
    {
        public int id = -1;
        public string name = "";
        public string accessRights = "";
        public bool isStaff = false;

        public Role() { }

        public Role(DataRow dr)
        {
            id = dr.Field<int>("ID");
            name = dr.Field<string>("NAME");
            accessRights = dr.Field<string>("ACCESS_RIGHTS");
            isStaff = dr.Field<bool>("IS_STAFF");
            Init();
        }

        public override string ToString()
        {
            return $"[Role] {id} {name} {accessRights} {isStaff}";
        }
    }
}
