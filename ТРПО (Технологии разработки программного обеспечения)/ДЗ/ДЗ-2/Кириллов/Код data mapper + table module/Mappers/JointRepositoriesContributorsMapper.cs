﻿using System.Data;
using Npgsql;
using System.Linq;
using System;
using TableModule.Entities;

namespace TableModule.Mappers
{
    public class JointRepositoriesContributorsMapper : MapperBase<JointRepositoriesContributors>
    {
        private static class CreateJointRepositoriesContributorsUtils
        {
            public static JointRepositoriesContributors Constructor(DataRow dr)
            {
                return new JointRepositoriesContributors(dr);
            }
        }

        // Update намеренно не реализован.
        public JointRepositoriesContributorsMapper(string connectionString) : base(connectionString, "\"REPOSITORIES_CONTRIBUTORS\"", CreateJointRepositoriesContributorsUtils.Constructor) {}

        public override int Create(JointRepositoriesContributors jrc)
        {
            var connection = new NpgsqlConnection(_connectionString);
            connection.Open();
            var cmd = new NpgsqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = $"INSERT INTO {_tableName} (\"CONTRIBUTORS_ID\", \"REPOSITORIES_ID\") VALUES ('{jrc.contributorId}', '{jrc.repositoryId}');";
            var flag = cmd.ExecuteNonQuery();
            jrc.Init();
            return flag;
        }

        public new JointRepositoriesContributors Read(int id)
        {
            Console.WriteLine($"У {_tableName} составной id. Пользуйтесь методом-перегрузкой");
            return null;
        }

        public JointRepositoriesContributors Read(int repoID, int contributorId)
        {
            string filter = $"CONTRIBUTORS_ID = {contributorId} AND REPOSITORIES_ID = {repoID}";
            return DataRow2Entity(_ReadAll().Select(filter).FirstOrDefault());
        }

        public override int Delete(JointRepositoriesContributors jrc)
        {
            var connection = new NpgsqlConnection(_connectionString);
            connection.Open();
            var cmd = new NpgsqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = $"DELETE FROM {_tableName} WHERE \"CONTRIBUTORS_ID\" = {jrc.contributorId} AND \"REPOSITORIES_ID\" = {jrc.repositoryId}";
            var flag = cmd.ExecuteNonQuery();
            jrc.Delete();
            return flag;
        }

        public int[] GetRepositoryContributors(int repoID)
        {
            string filter = $"REPOSITORIES_ID = {repoID}";
            var table = this._ReadAll();
            var dataRows = table.Select(filter);

            int[] contributors = new int[dataRows.Length];
            for (int i = 0; i < contributors.Length; i++)
            {
                contributors[i] = dataRows[i].Field<int>("CONTRIBUTORS_ID");
            }

            return contributors;
        }
    }
}
