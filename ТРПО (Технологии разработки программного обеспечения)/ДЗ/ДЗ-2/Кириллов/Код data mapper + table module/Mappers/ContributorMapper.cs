﻿using System.Data;
using Npgsql;
using TableModule.Entities;
using System;
using System.Linq;

namespace TableModule.Mappers
{
    public class ContributorMapper : MapperBase<Contributor>
    {
        private static class CreateContributorUtils
        {
            public static Contributor Constructor(DataRow dr)
            {
                return new Contributor(dr);
            }
        }

        public ContributorMapper(string connectionString): base(connectionString, "\"CONTRIBUTORS\"", CreateContributorUtils.Constructor) {}

        public override int Create(Contributor contributor)
        {
            var connection = new NpgsqlConnection(_connectionString);
            connection.Open();
            var cmd = new NpgsqlCommand();
            cmd.Connection = connection;
            DateTime dt = DateTime.Now;
            cmd.CommandText = $"INSERT INTO {_tableName} (\"CREATED\", \"ROLE_ID\", \"USER_ID\") VALUES ('{dt.ToString()}', '{contributor.roleId}', '{contributor.userId}');";
            var flag = cmd.ExecuteNonQuery();
            var created = FindByUserIdAndRole(contributor.roleId, contributor.userId);
            contributor.id = created.id;
            contributor.created = created.created;
            contributor.Init();
            return flag;
        }

        public override int Update(Contributor contributor)
        {
            var connection = new NpgsqlConnection(_connectionString);
            connection.Open();
            var cmd = new NpgsqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = $"UPDATE {_tableName} SET \"ROLE_ID\"={contributor.roleId}, \"USER_ID\"={contributor.userId} WHERE \"ID\"={contributor.id};";
            return cmd.ExecuteNonQuery();
        }

        public override int Delete(Contributor contributor)
        {
            var connection = new NpgsqlConnection(_connectionString);
            connection.Open();
            var cmd = new NpgsqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = $"DELETE FROM {_tableName} WHERE \"ID\"={contributor.id}";
            var flag =  cmd.ExecuteNonQuery();
            contributor.Delete();
            return flag;
        }

        public Contributor FindByUserIdAndRole(int roleId, int userId)
        {
            using NpgsqlCommand connection = new NpgsqlCommand(_connectionString);
            string filter = $"ROLE_ID = '{roleId}' AND USER_ID='{userId}'";
            return DataRow2Entity(_ReadAll().Select(filter).FirstOrDefault());
        }
    }
}
