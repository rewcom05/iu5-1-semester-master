﻿using System;
using System.Data;
using Npgsql;
using System.Linq;
using TableModule.Entities;

namespace TableModule.Mappers
{
    public enum RoleNames {
        manager,
        developer,
        sre,
    }

    public class RoleMapper: MapperBase<Role>
    {
        private static class CreateRoleUtils
        {
            public static Role Constructor(DataRow dr)
            {
                return new Role(dr);
            }
        }

        // Операторы Create, Update намерено не реализованы. Оператор Delete намеренно обезврежен.
        public RoleMapper(string connectionString) : base(connectionString, "\"ROLES\"", CreateRoleUtils.Constructor) {}

        public new int Delete(Role role)
        {
            Console.WriteLine("Запрещено удалять роли. Попытка удалить" + role.id);
            return 0;
        }

        public Role FindByName(string name)
        {
            using NpgsqlCommand connection = new NpgsqlCommand(_connectionString);
            string filter = $"NAME = '{name}'";
            return DataRow2Entity(_ReadAll().Select(filter).FirstOrDefault());

        }

        public Role FindByName(RoleNames role)
        {
            return FindByName(RoleToString(role));
        }

        protected string RoleToString(RoleNames role)
        {
            if (role == RoleNames.developer)
            {
                return "developer";
            }
            else if (role == RoleNames.manager)
            {
                return "manager";
            }
            else
            {
                return "sre";
            }
        }
    }
}
