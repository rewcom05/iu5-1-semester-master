﻿using System.Data;
using Npgsql;
using System.Linq;
using TableModule.Entities;

namespace TableModule.Mappers
{
    public class RepositoryMapper : MapperBase<Repository>
    {
        private static class CreateRepositoryUtils
        {
            public static Repository Constructor(DataRow dr)
            {
                return new Repository(dr);
            }
        }

        public RepositoryMapper(string connectionString): base(connectionString, "\"REPOSITORIES\"", CreateRepositoryUtils.Constructor) {}

        public override int Create(Repository repository)
        {
            var connection = new NpgsqlConnection(_connectionString);
            connection.Open();
            var cmd = new NpgsqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = $"INSERT INTO {_tableName} (\"NAME\") VALUES ('{repository.name}');";
            var flag = cmd.ExecuteNonQuery();
            var created = FindByName(repository.name);
            repository.id = created.id;
            repository.Init();
            return flag;
        }

        public override int Update(Repository repository)
        {
            var connection = new NpgsqlConnection(_connectionString);
            connection.Open();
            var cmd = new NpgsqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = $"UPDATE {_tableName} SET \"NAME\"='{repository.name}' WHERE \"ID\"={repository.id};";
            return cmd.ExecuteNonQuery();
        }

        public override int Delete(Repository repository)
        {
            var connection = new NpgsqlConnection(_connectionString);
            connection.Open();
            var cmd = new NpgsqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = $"DELETE FROM {_tableName} WHERE \"ID\"={repository.id}";
            var flag = cmd.ExecuteNonQuery();
            repository.Delete();
            return flag;
        }

        public Repository FindByName(string name)
        {
            using NpgsqlCommand connection = new NpgsqlCommand(_connectionString);
            string filter = $"NAME = '{name}'";
            return DataRow2Entity(_ReadAll().Select(filter).FirstOrDefault());
        }
    }
}
