﻿using System.Data;
using Npgsql;
using System.Linq;
using TableModule.Entities;

namespace TableModule.Mappers
{
    public class UserMapper: MapperBase<User>
    {
        private static class CreateUserUtils
        {
            public static User Constructor(DataRow dr)
            {
                return new User(dr);
            }
        }

        public UserMapper(string connectionString): base(connectionString, "\"USERS\"", CreateUserUtils.Constructor) {}

        public override int Create(User user)
        {
            var connection = new NpgsqlConnection(_connectionString);
            connection.Open();
            var cmd = new NpgsqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = $"INSERT INTO {_tableName} (\"LOGIN\", \"PASSWORD\", \"FIO\") VALUES ('{user.login}', '{user.password}', '{user.fio}');";
            var flag = cmd.ExecuteNonQuery();
            var created = FindByLogin(user.login);
            user.id = created.id;
            user.Init();
            return flag;
        }

        public override int Update(User user)
        {
            var connection = new NpgsqlConnection(_connectionString);
            connection.Open();
            var cmd = new NpgsqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = $"UPDATE {_tableName}" +
            $" SET \"LOGIN\"='{user.login}'," +
            $" \"PASSWORD\"='{user.password}'," +
            $" \"FIO\"='{user.fio}'" +
            $" WHERE id = {user.id}";
            return cmd.ExecuteNonQuery();
        }

        public override int Delete(User user)
        {
            var connection = new NpgsqlConnection(_connectionString);
            connection.Open();
            var cmd = new NpgsqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = $"DELETE FROM {_tableName} WHERE \"ID\"={user.id}";
            var flag = cmd.ExecuteNonQuery();
            user.Delete();
            return flag;
        }

        public User FindByLogin(string login)
        {
            using NpgsqlCommand connection = new NpgsqlCommand(_connectionString);
            string filter = $"LOGIN = '{login}'";
            return DataRow2Entity(_ReadAll().Select(filter).FirstOrDefault());
        }
    }
}
