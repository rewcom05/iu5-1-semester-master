﻿using System;
using System.Data;
using Npgsql;
using System.Linq;

namespace TableModule.Mappers
{
    public enum RoleNames {
        manager,
        developer,
        sre,
    }

    public class RoleMapper: MapperBase
    {
        public RoleMapper(string connectionString) : base(connectionString, "\"ROLES\"") { }
        
        public new int Delete(int id)
        {
            Console.WriteLine("Запрещено удалять роли");
            return 0;
        }

        public DataRow FindByName(string name)
        {
            using (NpgsqlCommand connection = new NpgsqlCommand(_connectionString))
            {
                string filter = $"NAME = '{name}'";
                return ReadAll().Select(filter).FirstOrDefault();
            }

        }

        public DataRow FindByName(RoleNames role)
        {
            return FindByName(RoleToString(role));
        }

        protected string RoleToString(RoleNames role)
        {
            if (role == RoleNames.developer)
            {
                return "developer";
            } else if (role == RoleNames.manager)
            {
                return "manager";
            } else
            {
                return "sre";
            }
        }
    }
}
