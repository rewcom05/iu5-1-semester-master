﻿using System.Data;
using Npgsql;
using System.Linq;
using System;

namespace TableModule.Mappers
{
    public class ContributorMapper : MapperBase
    {
        public ContributorMapper(string connectionString): base(connectionString, "\"CONTRIBUTORS\"")
        {
        }

        public int Create(int roleId, int userId)
        {
            var connection = new NpgsqlConnection(_connectionString);
            connection.Open();
            var cmd = new NpgsqlCommand();
            cmd.Connection = connection;
            DateTime dt = DateTime.Now;
            cmd.CommandText = $"INSERT INTO {_tableName} (\"CREATED\", \"ROLE_ID\", \"USER_ID\") VALUES ('{dt.ToShortDateString()}', '{roleId}', '{userId}');";
            return cmd.ExecuteNonQuery();
        }

        /*
        public DataRow Find(int roleId, int userId)
        {
            using (NpgsqlCommand connection = new NpgsqlCommand(_connectionString))
            {
                string filter = $"ROLE_ID = '{roleId}' AND USER_ID='{userId}'";
                return ReadAll().Select(filter).FirstOrDefault();
            }

        }
        */
    }
}
