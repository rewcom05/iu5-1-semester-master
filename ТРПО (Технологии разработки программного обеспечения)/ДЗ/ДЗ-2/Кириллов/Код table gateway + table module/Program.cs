﻿using System;
using Npgsql;
using TableModule.Modules;
using TableModule;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var connectionString = "Host=localhost;Username=postgres;Password=1407;Database=trpo";

                using var connection = new NpgsqlConnection(connectionString);
                connection.Open();
                var cmd = new NpgsqlCommand("SELECT version()", connection);
                var version = cmd.ExecuteScalar().ToString();
                Console.WriteLine($"PostgreSQL version: {version}");


                UserModule userModule = new UserModule(connectionString);
                RoleModule roleModule = new RoleModule(connectionString);
                ContributorModule contributorModule = new ContributorModule(connectionString);

                Console.WriteLine("Таблица USERS до выполнения скрипта");
                Console.WriteLine(userModule.ToString());
                Console.WriteLine();

                Console.WriteLine("Таблица ROLES до выполнения скрипта");
                Console.WriteLine(roleModule.ToString());
                Console.WriteLine();

                Console.WriteLine("Таблица CONTRIBUTORS до выполнения скрипта");
                Console.WriteLine(contributorModule.ToString());
                Console.WriteLine();


                Console.WriteLine("Пример 1. Поиск записи о пользователе");
                var dskirillov = userModule.FindByLogin("dskirillov");
                Console.WriteLine("Информация о Кириллове: " + Utils.DataRowToString(dskirillov));

                Console.WriteLine("Пример 2. Создание записи CONTRIBUTORS");
                var developerRole = roleModule.FindByName("developer");
                var dskirillovId = Convert.ToInt32(dskirillov["ID"]);
                var developerRoleId = Convert.ToInt32(developerRole["ID"]);
                contributorModule.Create(developerRoleId, dskirillovId);
                Console.WriteLine("Таблица CONTRIBUTORS после добавления developer'а");
                Console.WriteLine(contributorModule.ToString());
                Console.WriteLine();
                var createdRecord = contributorModule.Find(dskirillovId);
                Console.WriteLine("Поиск новой записи " + Utils.DataRowToString(createdRecord));
                Console.WriteLine();
                contributorModule.Delete(dskirillovId);
                Console.WriteLine("Таблица CONTRIBUTORS после удаления developer'а");
                Console.WriteLine(contributorModule.ToString());
                Console.WriteLine();

            }
            catch (Exception e)
            {
                Console.WriteLine($"Processing failed: {e.Message}");
                throw;
            }
        }
    }
}
