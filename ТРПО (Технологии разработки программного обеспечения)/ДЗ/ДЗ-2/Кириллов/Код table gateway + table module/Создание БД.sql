-- Database: trpo

-- DROP DATABASE IF EXISTS trpo;

CREATE DATABASE trpo
    WITH
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'Russian_Russia.1251'
    LC_CTYPE = 'Russian_Russia.1251'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

COMMENT ON DATABASE trpo
    IS 'ДЗ-ТРПО';

-- Обязательно! Для доступа из кода
GRANT ALL PRIVILEGES ON DATABASE trpo TO postgres;
