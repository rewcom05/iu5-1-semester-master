﻿using System.Data;
using System.Linq;
using Npgsql;

namespace TableModule.Modules
{
    public abstract class ModuleBase<Mapper> where Mapper : Mappers.MapperBase
    {
        protected Mapper _mapper;

        protected ModuleBase(Mapper mapper)
        {
            _mapper = mapper;
        }

        protected T GetColumn<T>(int id, string columnName)
        {
            return (T)GetRow(id)[columnName];
        }

        protected DataRow GetRow(int id)
        {
            return _mapper.Read(id);
        }
        
        protected DataTable GetTable()
        {
            return _mapper.ReadAll();
        }

        public override string ToString()
        {
            return _mapper.ToString();
        }
    }
}