﻿using Npgsql;
using System;
using System.Data;
using System.Linq;

namespace TableModule.Modules
{
    public class RepositoryModule : ModuleBase<Mappers.RepositoryMapper>
    {

        JointRepositoriesContributorsModule _jrcModule;
        ContributorModule _contributorModule;
        RoleModule _roleModule;

        public RepositoryModule(string connectionString, ContributorModule contributorModule, RoleModule roleModule) : base(new Mappers.RepositoryMapper(connectionString)) {
            _jrcModule = new JointRepositoriesContributorsModule(connectionString);
            _contributorModule = contributorModule;
            _roleModule = roleModule;
        }
        
        public int Create(string name)
        {
            return _mapper.Create(name);
        }

        public int Update(string name)
        {
            return _mapper.Update(name);
        }

        public int Delete(int id)
        {
            return _mapper.Delete(id);
        }

        public DataRow FindByName(string name)
        {
            return _mapper.FindByName(name);
        }

        public DataRow[] GetContributors(int id)
        {
            var contributorsIds = _jrcModule.GetRepositoryContributors(id);
            DataRow[] contributors = new DataRow[contributorsIds.Length];
            for (int i = 0; i < contributorsIds.Length; i++)
            {
                contributors[i] = _contributorModule.Find(contributorsIds[i]);
            }
            return contributors;
        }

        public DataRow GetManager(int id)
        {
            var contributors = GetContributors(id);
            var managerRoleId = _roleModule.FindByName("managerRole").Field<int>("ID");
            for (int i = 0; i < contributors.Length; i++)
            {
                var contributorRole = contributors[i].Field<int>("ROLE_ID");

                if (contributorRole == managerRoleId)
                {
                    return contributors[i];
                }
            }
            return null;
            // return new DataRow();
        }
    }
}
