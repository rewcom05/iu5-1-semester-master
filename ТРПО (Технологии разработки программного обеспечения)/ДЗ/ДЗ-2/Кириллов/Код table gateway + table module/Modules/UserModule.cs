﻿using Npgsql;
using System;
using System.Data;
using System.Linq;

namespace TableModule.Modules
{
    public class UserModule : ModuleBase<Mappers.UserMapper>
    {
        public UserModule(string connectionString) : base(new Mappers.UserMapper(connectionString)) { }

        
        public DataRow FindByLogin(string login)
        {
            return _mapper.FindByLogin(login);
        }

        public string getFIO(int id)
        {
            return GetColumn<string>(id, "FIO");
        }

        public string getLogin(int id)
        {
            return GetColumn<string>(id, "LOGIN");
        }

        public string getPassword(int id)
        {
            return GetColumn<string>(id, "PASSWORD");
        }


    }
}
