﻿using Npgsql;
using System;
using System.Data;
using System.Linq;

namespace TableModule.Modules
{
    public class RoleModule : ModuleBase<Mappers.RoleMapper>
    {
        public RoleModule(string connectionString) : base(new Mappers.RoleMapper(connectionString)) { }
        
        public DataRow FindByName(string name)
        {
            return _mapper.FindByName(name);
        }

        public string RoleToString(int id)
        {
            return GetColumn<string>(id, "NAME");
        }
    }
}
